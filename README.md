# basalte.rocks argos widget

Clone this repo, link python file to ~/.config/argos/
Make sure file is executable

If using gnome 3.36+ and the menu is not collapsing: check out this argos issue: https://github.com/p-e-w/argos/issues/114

![screenshot](screenshot.png "Screenshot")