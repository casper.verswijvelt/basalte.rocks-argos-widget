#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import requests
import json
import requests
import logging
import traceback
import spotipy
import sys
import os.path
import six
import datetime
import base64

if not six.PY2:
    from html import escape as html_escape
else:
    from cgi import escape as html_escape

from spotipy.oauth2 import SpotifyClientCredentials

def current_milli_time(): return int((datetime.datetime.now() -
                                      datetime.datetime(1970, 1, 1)).total_seconds() * 1000)

def get_as_base64(url):
    return base64.b64encode(requests.get(url).content)

def print_footer():
  print("---")
  print("<i>Open basalte.rocks</i> | href=https://basalte.rocks/party/  | iconName=go-home")
  if tracking == "1":
    print("<i>Stop tracking</i> | bash='" +
        sys.argv[0] + " 0' | refresh=true | terminal=false | iconName=media-playback-stop")
  else:
    print("<i>Start tracking</i> | bash='" +
          sys.argv[0] + " 1' | refresh=true | terminal=false | iconName=media-playback-start")
  print("<i>Refresh</i> | refresh=true | iconName=view-refresh")

tracking = "0"

stateFile = os.path.dirname(os.path.realpath(__file__)) + "/basalte.rocks.argos.state"

# Command line invocation
with open(stateFile, "a+") as f:
  if len(sys.argv) > 1:
    f.truncate(0)
    f.write(str(sys.argv[1]))
    f.flush()
    exit()

# Argos invocation
with open(stateFile) as f:
    tracking = f.read()

if tracking != "1":
    print("♫")
    print_footer()
    exit()


def msToTimeString(millis, shorten=False):
    seconds = (millis/1000) % 60
    seconds = int(seconds)
    minutes = (millis/(1000*60)) % 60
    minutes = int(minutes)
    hours = (millis/(1000*60*60)) % 24
    hours = int(hours)
    if shorten is True:
      if hours is 0:
        return "%02d:%02d" % (minutes, seconds)
    return "%02d:%02d:%02d" % (hours, minutes, seconds)

def progressBar(progress, length):
  result = ""
  pos = int(round(length * progress))

  for n in range(length):
    if n < pos:
      result = result + "■"
    elif n is pos:
      result = result + "▨"
    else:
      result = result + "□"
  return result

try:
    partyId = 'a50b8964-7ddc-45d7-8427-b90480ce5667'
    urlString = 'https://basalte.rocks/graphql'
    partyPassword = 'basalteforever'
    headers = {
      "X-Api-Key": "da2-3idbtpu7mbbjrcfqcwqqne3qym", 
      "Content-Type": "application/json"
    }
    data = {
      "operationName": "GetParty", 
      "variables": {
        "partyId": partyId,
        "password": partyPassword
      },
      "query": """query GetParty($partyId: ID!, $password: String!) {
        getParty(partyId: $partyId, password: $password) {
          currentTrack {
            track {
              name
              artists
              durationMs
              id
              album {
                name
                images {
                  width
                  height
                  url
                }
              }
            }
            progressMs
          }
          listenerCount
          playlist {
            tracks {
              id,
              votes
            }
          }
        }
      }"""
    }
    response = requests.post(urlString, headers=headers, data=json.dumps(data))


    if not response.ok:
      print("Error fetching party data")
      print("---")
      print(response.status_code)
      print(response.text)
      print_footer()
      exit()

    try:
      obj = json.loads(response.content)
    except Exception as e:
      print("Error parsing party data")
      print("--")
      print(str(e))
      print_footer()
      exit()

    if ((not "data" in obj) and (not "getParty" in obj["data"])):
      print("Error extracting party data")
      print_footer()
      exit()

    progressMs = 0
    durationMs = 0

    isPlaying = False
    hasPlaylist = False
    hasNextUp = False
    hasListenerStats = False

    if "currentTrack" in obj["data"]["getParty"] and obj["data"]["getParty"]["currentTrack"] is not None and obj["data"]["getParty"]["currentTrack"]["track"] is not None:

        isPlaying = True

        track = obj["data"]["getParty"]["currentTrack"]["track"]
        name = track["name"]
        artist = ", ".join(track["artists"])
        album = track["album"]["name"]
        id = track["id"]
        art = track["album"]["images"][1]["url"]
        progressMs = int(obj["data"]["getParty"]["currentTrack"]["progressMs"])
        durationMs = int(obj["data"]["getParty"]["currentTrack"]["track"]["durationMs"])

    if "playlist" in obj["data"]["getParty"] and "tracks" in obj["data"]["getParty"]["playlist"]:

        try:
          tracks = obj["data"]["getParty"]["playlist"]["tracks"]

          client_credentials_manager = SpotifyClientCredentials(
              "cef990c103e5481e89c72d3bb5a677d6", "11ee9dc1fe8f44a0a8163c5e167f3339")
          sp = spotipy.Spotify(
              client_credentials_manager=client_credentials_manager)

          ids = []
          votes = []

          for playlistTrack in tracks:
              votes.append(str(len(playlistTrack["votes"])))
              ids.append(playlistTrack["id"])

          ids = ids[:50]
          votes = votes[:50]
          spTracks = sp.tracks(ids)['tracks']

          if len(tracks) > 1:
              hasNextUp = True
              nextUpId = tracks[1]["id"]
              nextUpVotes = tracks[1]["votes"]
              nextUp = spTracks[1]

          hasPlaylist = True
        except:
          pass

    if "listenerCount" in obj["data"]["getParty"]:

      try:
        listeners = obj["data"]["getParty"]["listenerCount"]
        hasListenerStats = True
      except:
        pass

    
    # try:
    #   icecastResponse = requests.get(
    #   "https://stream.basalte.be:8443/status-json.xsl", verify=False)

    #   if icecastResponse.ok:
    #     icecastJson = json.loads(icecastResponse.content)

    #     #TODO check if these keys actually exist
    #     listeners = str(icecastJson["icestats"]["source"]["listeners"])
    #     peakListeners = str(icecastJson["icestats"]["source"]["listener_peak"])

    #     hasListenerStats = True
    # except Exception:
    #   pass

    if isPlaying:
        title = name + " - " + artist
        print(html_escape((title[:50] + ' ...') if len(title) > 54 else title))
        print("---")
        print("| imageWidth=250 image=" + str(get_as_base64(art)).replace("b'", "").replace("'",""))
        print(msToTimeString(progressMs, True) + " " + progressBar(progressMs/durationMs, 15) + " " + msToTimeString(durationMs, True))
        print("---")
        print("<b>Artist: </b>" + html_escape(artist) +
              " | href=https://open.spotify.com/artist/" + spTracks[0]['artists'][0]['id'])
        print("<b>Album: </b>" + html_escape(album) +
              " | href=https://open.spotify.com/album/" + spTracks[0]['album']['id'])
        print("<b>Song: </b>" + html_escape(name) +
              " | href=https://open.spotify.com/track/" + id)
        print("<b>Votes: </b>" + str(len(tracks[0]["votes"])))

    else:
        print("Nothing is playing on basalte.rocks")

    if hasNextUp:
        print("---")
        print("<b>Next up: </b>" + html_escape(nextUp["name"] + " - " + nextUp['artists']
                                               [0]['name']) + " | href=https://open.spotify.com/track/" + nextUp['id'])
        print("<b>Votes: </b>" + str(len(nextUpVotes)))

    if hasPlaylist:
        print("---")
        print("<b>Queue: </b>" + str(len(spTracks)) + " track" + ("s" if len(spTracks) != 1 else ""))

        songStartMs = current_milli_time() - progressMs
        counter = 0
        for track in spTracks:
            print(html_escape("-- " + msToTimeString(songStartMs) + "\t" +
                              votes[counter] + "\t " + track['name'] + ' - ' + track['artists'][0]['name']) + " | href=https://open.spotify.com/track/" + track['id'])
            songStartMs = songStartMs + track["duration_ms"]
            counter = counter + 1
    if hasListenerStats:
      print("---")
      print("<b>Listeners: </b>" + str(listeners))
      # print("<b>Peak listeners: </b>" + str(peakListeners) +
      #       " | href=https://stream.basalte.be:8443/")
except Exception as e:
    print("Error")
    print("---")
    print("Something went wrong while retrieving basalte.rocks current song data")
    print("<i> -> " + str(e) + "</i>")
    logging.error(e)
    logging.error(e, exc_info=True)

print_footer()
